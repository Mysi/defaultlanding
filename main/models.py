from django.db import models


# Create your models here.
class Page(models.Model):
    title = models.CharField(max_length=32)
    body = models.TextField()
    url = models.URLField(null=True, blank=True)


class ImpKey(models.Model):
    ip = models.CharField(max_length=256)
    key = models.CharField(max_length=256)
    end = models.DateTimeField()
