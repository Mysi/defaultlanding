import datetime
import random
import string

from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.
from django.utils.safestring import mark_safe
from django.views.decorators.csrf import csrf_exempt

from main.models import Page, ImpKey


@csrf_exempt
def main(request, path=None):
    try:
        page = Page.objects.get(url="http://" + request.get_host() + request.path)
    except Page.DoesNotExist:
        page = Page.objects.get(url=None)

    return render(request, "base.html", {"title": page.title, "body": mark_safe(page.body)})


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

@csrf_exempt
def impressum(request):
    keyObj = ImpKey.objects.filter(ip=get_client_ip(request), end__gt=datetime.datetime.now()).last()
    if keyObj and keyObj.key == request.GET.get("session"):
        return render(request, "impressum.html", {})
    elif request.GET.get("create_session") == "696969696969":
        key = "".join(random.choices(string.ascii_uppercase + string.digits, k=150))
        new_obj = ImpKey()
        new_obj.key = key
        new_obj.ip = get_client_ip(request)
        new_obj.end = datetime.datetime.now() + datetime.timedelta(hours=1)
        new_obj.save()
        return HttpResponse(f'{{"session": "{key}"}}', content_type="application/json")
    else:
        return render(request, "impressum_req.html", {})
