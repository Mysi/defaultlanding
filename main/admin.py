from django.contrib import admin

# Register your models here.
from main.models import Page, ImpKey

admin.site.register(Page)
admin.site.register(ImpKey)
